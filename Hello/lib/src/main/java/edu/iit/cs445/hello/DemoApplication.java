package edu.iit.cs445.hello;

//import jakarta.ws.rs.*;
//import jakarta.ws.rs.core.*;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

@ApplicationPath("/api")
public class DemoApplication extends Application {
	// the entry class - deliberately empty in this basic demo
}

