package edu.iit.cs445.hello;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

// Pingable at http://localhost:8080/hello/api/demo/cat
//   hello:	the basename of the WAR file, see the gradle.build file
//   api:	see the @ApplicationPath annotation in LampDemo.java
//   demo:	see the @Path annotation *above* the REST_controller declaration in this file
//   lamps:	see the @Path declaration above the first @GET in this file

@Path("demo")
public class SayHello {
	@Path("cat")
	@GET
	public Response sayHello() {
		return Response.status(Response.Status.OK).entity("Meow\n").build();
	}
}
